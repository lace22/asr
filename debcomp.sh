#!/bin/bash
clear
SIZE=`du -sk deback/Debian.pcl | cut -f 1`
(sudo tar cvf - deback/Debian.pcl | pv -s ${SIZE}k  -n | gzip -c > deback/Debian.tar.gz) 2>&1 | dialog --gauge "Comprimint el backup" 10 70
if [ $? -eq 0 ]; then
    clear
    sudo rm -f Debian.pcl
    dialog --title "Sortir" --msgbox "$(echo 'Backup finalitzat amb exit, piqui "Enter" per sortir al menu principal')" 100 100
    bash main.sh
else
    dialog --title "Sortir" --msgbox "$(echo 'Alguna cosa ha sortit malament, sortint...')" 100 100
    bash main.sh
fi   
