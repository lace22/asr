#!/bin/bash
$?=1
clear
SIZE=`21571132`
(gunzip -c deback/Debian.tar.gz | pv -s 15571132k  -n | tar xvf - )2>&1 | dialog --gauge "Descomprimint el backup" 10 70

if [ $? -eq 0 ]; then
    clear
    dialog --title "Sortir" --msgbox "$(echo 'Decompresió amb exit, hara començará la restauració')" 100 100
	bash debrestore.sh
else
    echo FAIL
fi   
