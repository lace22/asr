#!/bin/bash
dialog --backtitle "Reparación Linux"   \
       --menu "Què vols fer?" \
       10 30 3 \
       0 "Sortir" \
       1 "Restaurar Debian"  \
       2 "Fer backup"  \
       2>temp
if [ "$?" = "0" ]
then
        _return=$(cat temp)
 
        # Sortir
        if [ "$_return" = "0" ]
        then
          clear
        	bash main.sh
          rm -f temp

        fi
 
       
        if [ "$_return" = "1" ]
        then
        	clear
			    bash debdec.sh
        fi
    
        if [ "$_return" = "2" ]
        then
        	bash debclone.sh
        fi
else
  rm -f temp
  clear
fi