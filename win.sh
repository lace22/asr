#!/bin/bash
dialog --backtitle "Reparación Windows"   \
       --menu "Què vols fer?" \
       10 30 3 \
       0 "Sortir" \
       1 "Restaurar Windows"  \
       2 "Fer backup"  \
       2>temp
if [ "$?" = "0" ]
then
        _return=$(cat temp)
 
        # Sortir
        if [ "$_return" = "0" ]
        then
        	bash main.sh
          clear
          rm -f temp

        fi
 
       
        if [ "$_return" = "1" ]
        then
          clear
			     bash windec.sh
        fi
    
        if [ "$_return" = "2" ]
        then
          clear
        	bash winclone.sh
        fi
else
  rm -f temp
  clear
fi