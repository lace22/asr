#!/bin/bash
clear
dialog --backtitle "Automatic System Recovery"   \
       --menu "Què vols fer?" \
       10 30 3 \
       0 "Sortir" \
       1 "Reparar Windows"  \
       2 "Reparar Linux"  \
       2>temp 
 
 
# OK is pressed
if [ "$?" = "0" ]
then
        _return=$(cat temp)
 
        # Sortir
        if [ "$_return" = "0" ]
        then
                dialog --title "Sortir" --msgbox "$(echo 'Reiniciant l' ordinador)" 100 100
                clear
                rm -f temp
                sudo init 6
        fi
 
        # Restaurar el sistema operatiu Windows
        if [ "$_return" = "1" ]
        then
        	clear
			bash win.sh
        fi
        # Restaurar el sistema operatiu Windows
        if [ "$_return" = "2" ]
        then
        	clear
            bash deb.sh
        fi
	 
# Cancel or ESC
else
        # Reiniciem el PC
        echo "Sortint"
        clear
fi


 
# remove the temp file
rm -f temp
