#!/bin/bash
clear
SIZE=`du -sk deback/Windows.pcl | cut -f 1`
(sudo tar cvf - deback/Windows.pcl | pv -s ${SIZE}k  -n | gzip -c > deback/Windows.tar.gz) 2>&1 | dialog --gauge "Comprimint el backup" 10 70
if [ $? -eq 0 ]; then
    clear
    sudo rm -f Windows.pcl
    dialog --title "Sortir" --msgbox "$(echo 'Backup finalitzat amb exit, piqui "Enter" per sortir al menu principal')" 100 100
    rm -f Debian.pcl 
    bash main.sh
else
    dialog --title "Sortir" --msgbox "$(echo 'Alguna cosa ha sortit malament, sortint...')" 100 100
    bash main.sh
fi   
